let DTB = (...Score) => {
  let sum = 0;
  let average = 0;
  for (let i = 0; i < Score.length; i++) {
    sum += Score[i];
    average = sum / Score.length;
  }
  return average.toFixed(2);
};

let calcGroup1Score = () => {
  let math = document.getElementById("inpToan").value * 1;
  let physic = document.getElementById("inpLy").value * 1;
  let chemistry = document.getElementById("inpHoa").value * 1;
  let dtb = DTB(math, physic, chemistry);
  document.getElementById("tbKhoi1").innerText = dtb;
};

let calcGroup2Score = () => {
  let math = document.getElementById("inpVan").value * 1;
  let physic = document.getElementById("inpSu").value * 1;
  let chemistry = document.getElementById("inpDia").value * 1;
  let english = document.getElementById("inpEnglish").value * 1;
  let dtb = DTB(math, physic, chemistry, english);
  document.getElementById("tbKhoi2").innerText = dtb;
};
