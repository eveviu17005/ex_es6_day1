const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let renderColorList = () => {
  let contentHTML = "";
  colorList.forEach((color) => {
    contentHTML += `<button class="color-button ${color}"></button>`;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderColorList();

let changeHouseColor = () => {
  let chosenColor = document.querySelectorAll(".color-button");

  let colorArr = Array.from(chosenColor);
  let index = 0;

  colorArr.forEach((sv) => {
    sv.addEventListener("click", () => {
      index = colorArr.findIndex((x) => {
        return x == sv;
      });
      for (var i = 0; i < colorArr.length; i++) {
        let houseColor = `${colorList[i]}`;

        colorArr[i].classList.remove("active");
        document.getElementById("house").classList.remove(houseColor);
      }
      sv.classList.add("active");
      document.getElementById("house").classList.add(`${colorList[index]}`);
    });
  });
};

changeHouseColor();
